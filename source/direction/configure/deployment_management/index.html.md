---
layout: markdown_page
title: "Deployment Management Direction"
description: "Managing how deployment is conducted to higher tier environments is a major organizational challenge. This direction page communicates how GitLab thinks about this problem and the direction we intend to take."
canonical_path: "/direction/configure/deployment_management/"
---

- TOC
{:toc}

## Overview

Managing an organization's deployments is a major challenge. Particularly in larger organizations, with the proliferation of services, technologies, dependencies on one side, and compliance and security requirements on the other side, many teams find themselves struggling to deploy frequently and consistently in a repeatable manner. 

Platform teams want to help development teams become more efficient; so that they can meet compliance and security requirements and deploy to environments they can't (and probably shouldn't) update, without slowing down their ability to implement changes to their applications. 

## Mission

Help GitLab users to deploy in automated and standardized ways to any envrionment and target infrastructure.

## Vision

GitLab distrupts the market with a fully declarative, scalable, modular, testable approach to deployment management that supports any major target infrastructure from bare metal servers to container orchestrators to edge devices and mobile app stores. 

Declarative operations is to the devops pipeline what serverless is to infrastructure. It shifts the responsibility of operating the underlying systems to a service provider / controller, and enables the user to focus on their business instead.

The biggest difficulty with every automation tool is that it is code. As a result it requires developers to write it, a runtime environment to run it and a lot of investment into learning the tool chain. Compare this to the declarative nature of Kubernetes. With Kubernetes everyday developers realised that operations are complex, but operations were complex even before Kubernetes. Kubernetes, with its declarative, everything as data approach made this complexity approachable for every developer.

At the same time, Kubernetes made it clear that higher-level abstractions are required for developers without compromising the core flexibility of the container orchestrator.

We envision our solution to have several levels in terms of user value and complexity.

![Ease of use](./gitlab-delivery_ease-of-use.png) 

Today, we focus our efforts on the Gitlab Agent for Kubernetes. The agent provides great flexibility, but does not offer any out of the box, white-glove experiences for less experienced users. We want to add higher levels of abstractions on top of the agent. 

The second level, what I call GitLab Delivery here, includes various integations, but experienced users can pick only parts of our recommended tooling and they can build their own processes around it. This level should integrate well with other GitLab stages, especially around Package, Release and Secrets management. 

The third level, that is closes to the current Auto Deploy offering, provides a fully-managed solution for our users. It can approached from two different angles. First, from the software developers' point of view, it's a black box that should "just work" and provides a full, production ready delivery process. Second, from the platform engineers' point of view, it's a customized version of GitLab Delivery. That is it should be a framework to build custom delivery pipelines (not to be confused with CI pipelines) that result in white-glove solutions for the software engineer.

![Complexity](./gitlab-delivery_managed-integrations.png) 

As we increase the value managed by GitLab, we need to increase the complexity owned by GitLab. At the agent level, we are responsible for the core cluster connection only. To best understand the agent, one should think of it as not providing any user-level features. It is responsible for maintaining a bi-directional communication channel between a cluster and GitLab. We add various features on top of this channel, like pull and push-based deployments, security scanning, direct access of cluster resources from the GitLab UI, etc. At this level, we try avoid 3rd party integrations as much as possible to minimize our maintenance costs and build an extremely reliable foundation for higher-level features.

At the GitLab Delivery level, we need to pick at least a few tools and integrated those into our offering. The set of tools likely includes an ingress, a secrets management solution, a certificate management solution, support for advanced deployment strategies and integrations with GitLab Observability.

On top of GitLab Delivery, Auto DevOps requires even more integrations. At this stage the vision is that the user provides us a Dockerfile and we return them a URL where they can check out their application. On top of the integrations mentioned under GitLab Delivery, we would likely want to add integrations with even more specific tools, like Knative and a Service Mesh. Finally, expanding the scope of Auto Deploy to other deployment targets, mobile for example, likely requires a totally new set of integrations.

### Maturity

We consider Deployment Management to be at the minimal level. We believe that the current CI/CD based approach, environments, releases are acceptable for many users, but are far from a distruptive solution.

## Market

As CI became mainstream, we believe that the next big market is around integrated and scalable deployment solutions. The total addressable market (TAMkt) for DevOps tools targeting the Release and Configure stages was [$1.79B in 2020 and is expected to grow to $3.25B by 2024 (13.8% CAGR) (i)](https://docs.google.com/spreadsheets/d/1LO57cmXHDjE4QRf6NscRilpYA1mlXHeLFwM_tWlzcwI/edit?ts=5ddb7489#gid=1474156035). Continuous delivery alone, which does not include categories such as infrastructure as code, is estimated to have a market size of $1.62B in 2018 growing to $6B by 2026 (17.76% CAGR). 

Deployment Management enables an integrated, programmable approach to serve this market, and provides opportunities for GitLab in the future.

## Target persona

The primary persona is [the Platform Engineer](/handbook/product/personas/#priyanka-platform-engineer). Their job is to support all the development teams with standardised and compliant pipelines.

The secondary personas are [the Application Operator](/handbook/product/personas/#allison-application-ops) and [the Compliance Manager](/handbook/product/personas/#cameron-compliance-manager). The Application Operator is responsible for deploying and operating the business applications, while the compliance manager assures that all the processes follow internal policies.

### User story map

Deployments don't live in a vacuum, but are part of a companies delivery process. They are preceded and followed by CI jobs, are surrounded by security checks and guardrails, are integrated with monitoring and work in coordination with release management. We created [a user story map](https://app.mural.co/t/gitlab2474/m/gitlab2474/1642001078399/2dfb4a6a7307e42f2a2e42f0e10f49dc83bdfbae?wid=0-1647471145120) to highlight and discuss these cross-stage and intra-group jobs and integration requirements.

## Strategy

### Pricing
This section establishes the pricing alignment scaffolding for this category based on GitLab's [buyer-based tiering](https://about.gitlab.com/company/pricing/#buyer-based-tiering-clarification). 

#### Free
Managing deployments using existing CI/CD features, such as using `include` will continue to be free. For a large number of GitLab users, from individual developers to small companies, this is sufficient.

#### Premium
Orchestrating moderately complex deployments across a number of projects becomes hard to maintain and manage. Declarative management of multi-project deployment orchestration will fall under the Premium tier. These features enable development teams while keeping control within the platform team.

Features targeting a dedicated platform team fall under the Premium tier. While platform engineers are individual contributors, they do not contribute directly to the business. Their contribution is indirect, through increased developer productivity, security and reliability. Thus a platform teams assumes a non-IC decision maker who expects a level of standardization across several teams.

#### Ultimate
Helping organizations with a standards-based and compliant deployment pipeline is critically important that gets the attention of CIOs. Dashboards that summarize compliance and empower leaders with a birds-eye-view of their entire organization's change management solves a fundamental problem for GitLab's largest customers. 

Security guardrails and policy attestation support and reporting fall under this category.

### Current focus

Today, GitLab CI/CD is used by many Gitlab customers to manage their deployments. GitLab CI/CD is a mixture of imperative and declarative code that is hard to test and its YAML based syntax does not offer much possibility for scalable, modular setup. As deployments are complex processes where all the previous work and requirements need to converge having strong foundations is crucial.

As GitLab already provides support to deploy into non-production and production environments, we updated the GitLab Documentation to reflect this by moving existing content under "Use GitLab / Release Your Application" and renaming "Release Your Application" to "Deploy and Release Your Application"

### Next 6 months

- We are working on [defining the entities and processes around deployments](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/75338) to have a shared vocabulary to discuss deployment requirements.
- We want to improve support for industry standard Kubernetes-oriented deployment tools, like [Helm and Kustomize](https://gitlab.com/groups/gitlab-org/-/epics/7938).
- Together with the Release group, we are working on [showing Kubernetes-related, operational insights on Environment pages](https://gitlab.com/gitlab-org/gitlab/-/issues/352186).
- We want to provide [a simple, fully automated approach to deploy to non-production environments on top of Kubernetes](https://gitlab.com/groups/gitlab-org/-/epics/8874). This is a follow-up action as we deprecated the legacy [GitLab Managed Cluster](https://docs.gitlab.com/ee/user/project/clusters/gitlab_managed_clusters.html) functionality together with deprecating the certificate-based cluster connections.

### Next 6-18 months

- We want to lay down the foundations for a new Deployment Management project within GitLab together with standardizing around a set of supported tools for GitLab Delivery.
- We want to further improve the Kubernetes - Environments page integration by integrating with the [GitLab Observability stack](https://about.gitlab.com/direction/monitor/observability/) and building out a [feature-full Kubernetes dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2493)

## The Deployment management project

This section shares our current approach and plans towards a Deployment management project built-into GitLab. This direction is currently being researched and architected. It will very likely change. Your feedback is more than welcome!

A core building block of a Deployment management is a GitLab project to store the declarative deployment definitions in a structure following GitLab-defined conventions and the GitLab agent for Kubernetes, to consume the content stored in a GitLab Deployment management project. A Deployment management project is supported by GitLab as long as it follows the GitLab conventions. 

A note to help understand the management project: This approach we imagine is similar to various frontend tools, like "Create React App", "Expo" (React Native). These tools provide a simplified experience out of the box for 80% of the use cases. If the user needs more flexibility, they can [eject](https://create-react-app.dev/docs/available-scripts#npm-run-eject). Thus, the user owns their project, but a lot of complexity remains hidden from them to support them focusing on their business problem. This is aligned with [the convention over configuration product principle](https://about.gitlab.com/handbook/product/product-principles/#convention-over-configuration).

The Deployment management project should describe:
- a list of environments
- agents managing the deployments
- manifests, Helm charts for all the managed environments
- the deployment process, including how a deployments might be promoted between environments
- notification sinks on various deployment related events

The Deployment management project might include:
- secrets in encrypted form (e.g. with SOPS or Sealed Secrets)
- metrics descriptions
- release automation, optionally connected with metrics

We want to work closely with [the Release stage](https://about.gitlab.com/direction/release/) in shipping the Deployment management project. The Release stage is responsible for visualising [Environments](https://about.gitlab.com/direction/release/environment_management/) and related deployment instances.

## GitLab Delivery

This section shares our current approach and plans towards simplifying the Delivery experience for our users . This direction is currently being researched and architected. It will very likely change. Your feedback is more than welcome!

The GitLab Delivery framework is an opinionated selection of tools to support platform teams in providing white-glove delivery experiences to software developers.

| Ease of use | Complexity |
| --- | --- |
| ![Ease of use](./gitlab-delivery_ease-of-use.png)   | ![Complexity](./gitlab-delivery_managed-integrations.png)  |

The more user friendly we make a solution, the more complexity we become responsible for. Increasing complexity means increasing maintenance costs and higher customer value. 

The agent for Kubernetes is the core building block of GitLab Delivery. It is reliable, production ready tool for declarative deployment management of Kubernetes clusters built around GitOps principles. GitLab Delivery extends our product offering on top of the agent for Kubernetes.

The GitLab Delivery layer wants to offer a customizable selection of shared cluster tools from ingress to observability that we have integrated across GitLab. At the GitLab Delivery level, we need to pick at least a few tools and integrated those into our offering. The set of tools likely includes an ingress, a secrets management solution, a certificate management solution, support for advanced deployment strategies and integrations with GitLab Observability. This level should integrate well with other GitLab stages, especially around Package, Release and Secrets management. 

GitLab Delivery will be integrated with the Deployment management project. The two working together should support 

- a customizable framework to implement company-specific processes and policies for the platfrom engineers,
- fully automated and integrated delivery pipeline for the software engineering teams.

Nota bene: What we call GitLab Delivery here falls under the larger [GitLab Delivery direction](https://about.gitlab.com/direction/delivery/). The GitLab Delivery framework - as the name suggests - is a central component of the overall direction, but is not the direction itself. The scope of the Delivery direction relies heavily on the work on the Release stage to support release managers. The work on the Delivery framework needs to be aligned with the Release stage strategy as they supplement each other. Similarly, the Delivery framework work together with the Continuous Verifications category direction, that is part of the bigger Delivery direction.

## Competition

- [Keptn](https://keptn.sh/)
- [Argo project with Argo Rollouts](https://argoproj.github.io/)
- [Harness](https://harness.io/)

We [reviewed](https://gitlab.com/gitlab-org/gitlab/-/issues/352361) (internal) Keptn, Argo and Harness in early 2022.

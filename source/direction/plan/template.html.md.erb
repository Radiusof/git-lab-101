---
layout: markdown_page
title: Product Stage Direction - Plan
description: "The Plan stage enables teams to effectively plan features and projects in a single application"
canonical_path: "/direction/plan/"
---

Content last reviewed on 2022-01-19

- TOC
{:toc}

<p align="center">
    <font size="+2">
        <b>Enable teams to effectively plan and execute work in a single application</b>
    </font>
</p>

<%= partial("direction/plan/templates/overview") %>

<%= devops_diagram(["Plan"]) %>

## Stage Overview

The Plan Stage provides tools for teams to manage their work, track operational health and measure outcomes. As an end-to-end DevSecOps platform, GitLab is uniquely positioned to deliver a planning suite that enables business leaders to drive their vision and DevSecOps teams to deliver value while improving how they work. In addition, the unification of the DevSecOps process allows GitLab to interlink data across every stage of development, from initial analysis, to planning, implementation, deployment, and monitoring.

### Group and Categories

<%= partial("direction/plan/templates/categories") %>

## 3 Year Stage Themes
<%= partial("direction/plan/templates/themes") %>
 
## 3 Year Strategy

In three years, the Plan Stage market will:
* Continue to shift from project to product and focus on outcomes instead of output.
* Continue to move away from command and control mentality and instead empower teams to determine how they can contribute toward business objectives.
* Make operational efficiency and continual improvement a top priority.  
* Embrace machine learning and automation within the Plan stage of the DevSecOps toolchain and lifecycle.
* Shift toward consolidation into a single platform for all stages of the DevSecOps lifecycle.

As a result, in three years, Gitlab will:
* Provide support for individual DevSecOps teams and entire organizations using scaled Agile frameworks.
* Allow GitLab to capture and tie metrics to [Work Items](https://docs.gitlab.com/ee/development/work_items.html) to reflect business outcomes. 
* Surface metrics like DORA and Value Stream in key parts of a teams workflow to help drive improvements. 
* Support frameworks like OKRs that encourage bottom-up contributions. 
* Use downstream DevSecOps data for automation and machine learning to help teams improve their plans.
* Make it easy for non-Developer Personas to contribute and read and edit planning data in GitLab. 

## 1 Year Plan

### What We Recently Completed

* [Epic Linking](hhttps://gitlab.com/groups/gitlab-org/-/epics/7546) - Relating epics to each other with "Related", "Blocking" or "Blocked By" relationships. Enables users to manage cross team dependencies at the portfolio level. 
* New [Custom DORA reporting](https://about.gitlab.com/releases/2022/08/22/gitlab-15-3-released/#dora-custom-reporting-for-data-driven-software-development-improvements) for tracking metrics improvements, understand patterns in metrics trends, and compare performance between groups and projects.
* New [GraphQL API for Contribution Analytics](https://about.gitlab.com/releases/2022/11/22/gitlab-15-6-released/#new-graphql-api-for-contribution-analytics). To help users analyze team contributions, we have now exposed the [Contribution Analytics](https://docs.gitlab.com/ee/user/group/contribution_analytics/index.html) data through GraphQL. Using this new API, users can identify opportunities for improvement and gain insights into the top contributors in your team. 
* [Internal Notes](https://gitlab.com/groups/gitlab-org/-/epics/7407) - Individuals with the appropriate permissions level can now redact discussions with internal or customer data that should only be visible to certain people while keeping the core details about an issue public. Internal notes in issues or epics can only be seen by the issue author, assignee, and group or project members with at least the Reporter role.
<%= partial("direction/plan/project_management/team_planning/recent_accomplishments") %>
<%= partial("direction/plan/project_management/planning_analytics/recent_accomplishments") %>

### What We Are Currently Working On

* [Unified work item architecture and refactored/revamped UI/UX](https://gitlab.com/groups/gitlab-org/-/epics/6033) - A unified implementation for [Work Items](https://docs.gitlab.com/ee/development/work_items.html) will reduce rework, provide a more consistent experience, and allow us to introduce new functionality faster. 
   * Our first iteration of work items introduced [Tasks](https://gitlab.com/groups/gitlab-org/-/epics/7103) as a way to decompose Issues into smaller increments of work. We are now maturing Tasks to address user feedback and increase adoption.
   * We are migrating[requirements](https://gitlab.com/gitlab-org/gitlab/-/issues/323779) to the work items framework, which will increase maturity to Viable.
   * We are introducing [Objectives and Key Results](https://gitlab.com/groups/gitlab-org/-/epics/7864) work items to enable outcome-based strategic planning.
* Enable epics to have child items from [different top-level groups](https://gitlab.com/gitlab-org/gitlab/-/issues/205155) - Allowing child items from other top-level groups will enable a more accurate representation of work that crosses teams and organizational lines. This has been a common feedback item from large customers and a blocker for adopting portfolio planning at GitLab. 
* Improving performance will continue to be a top priority in the near term. The rapid growth of GitLab.com has uncovered the need for continue focus on [database and query optimization](https://gitlab.com/groups/gitlab-org/-/epics/5804).

<%= partial("direction/plan/templates/next") %>


### Cross-Stage Initiatives

Plan offers functionality that ties into workflows in other stages.  We are actively collaborating with other stages that are building upon Plan functionality to meet their users needs.

* The Manage stage has built a Jira integration that displays Jira Issue data within GitLab. We will collaborate with that team to tie Jira Issues into more workflows like reporting and tying Jira Issues to higher level work items.
* The Manage stage owns the [Jira importer](https://docs.gitlab.com/ee/user/project/import/jira.html) to allow Jira issues to be migrated to GitLab. We will continue to work with that team to extend GitLab work items to accomodate more critical data elements from Jira to ensure a seamless import process.
* The Plan:Project Management and Create:Editor groups will work together to incroporate the new WSWYG markdown editor into Work Items.
* The Plan and Manage stages are collaborating on the [Workspaces](https://gitlab.com/groups/gitlab-org/-/epics/6473) initiative to simplify the management of work and team hierarchies in GitLab.
* The Monitor:Respond group built Incidents based on Issues with guidance from the Plan:Project Management group. We will continue to collaborate as we move to our new architecture with Work Items.
* Plan:Project Management, Govern:Compliance, Verify, and Release are working together on CloudEvents and Eventing as part of Workflows & Automations validation (gitlab-org/gitlab#344136).

Please explore the individual [Category](https://about.gitlab.com/direction/plan/#categories) Direction pages for more information on 12 month plans.

### Target audience

<%= partial("direction/plan/templates/target_audience") %>


### Pricing

<%= partial("direction/plan/templates/pricing") %>

An example of what the end result data model and pricing could look like based on these pricing principles:

![Work Items Hierarchy](/images/direction/plan/workitemhierarchy.png)


### Jobs To Be Done 

[View the Plan stage JTBD](/direction/plan/jtbd.html)
